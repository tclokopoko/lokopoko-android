package com.lokopokotravel.model;

import java.math.BigDecimal;

public class Pricing {
	private int tourId;
	private String customerType; // eg Adult, Children, Infant & etc.
	private String dayType; // eg Weekday, Weekend, Special;
	private BigDecimal price;
	
	public Pricing(int tourId, String customerType, String dayType, BigDecimal price) {
		this.tourId = tourId;
		this.customerType = customerType;
		this.dayType = dayType;
		this.price = price;
	}
	
	public int getTourId() {
		return tourId;
	}
	public void setTourId(int tourId) {
		this.tourId = tourId;
	}
	public String getCustomerType() {
		return customerType;
	}
	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}
	public String getDayType() {
		return dayType;
	}
	public void setDayType(String dayType) {
		this.dayType = dayType;
	}
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
}
