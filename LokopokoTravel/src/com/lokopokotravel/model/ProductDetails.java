package com.lokopokotravel.model;

import java.math.BigDecimal;
import java.util.List;

public class ProductDetails extends Product {
	private List<String> imageUrls;
	private String adultRangeText;
	private String childRangeText;
	private String infantRangeText;
	private int maxNoPax;
	private BigDecimal depositAmount;
	
	public ProductDetails() { 
		super(); 
	}
	
	public ProductDetails(Product product, List<String> imageUrls, String adultRangeText,
			String childRangeText, String infantRangeText, int maxNoPax,
			BigDecimal depositAmount) {
		
		super(product.getProductId(), product.getProductName(), product.getProductType(), product.getProductLowestPrice()
				, product.getProductRating(), product.getProductDefaultImageUrl());
		this.imageUrls = imageUrls;
		this.adultRangeText = adultRangeText;
		this.childRangeText = childRangeText;
		this.infantRangeText = infantRangeText;
		this.maxNoPax = maxNoPax;
		this.depositAmount = depositAmount;
	}
	
	public List<String> getImageUrls() {
		return imageUrls;
	}

	public void setImageUrls(List<String> imageUrls) {
		this.imageUrls = imageUrls;
	}

	public String getAdultRangeText() {
		return adultRangeText;
	}

	public void setAdultRangeText(String adultRangeText) {
		this.adultRangeText = adultRangeText;
	}

	public String getChildRangeText() {
		return childRangeText;
	}

	public void setChildRangeText(String childRangeText) {
		this.childRangeText = childRangeText;
	}

	public String getInfantRangeText() {
		return infantRangeText;
	}

	public void setInfantRangeText(String infantRangeText) {
		this.infantRangeText = infantRangeText;
	}

	public int getMaxNoPax() {
		return maxNoPax;
	}

	public void setMaxNoPax(int maxNoPax) {
		this.maxNoPax = maxNoPax;
	}

	public BigDecimal getDepositAmount() {
		return depositAmount;
	}

	public void setDepositAmount(BigDecimal depositAmount) {
		this.depositAmount = depositAmount;
	}
}
