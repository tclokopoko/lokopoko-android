package com.lokopokotravel.model;

import java.math.BigDecimal;

import android.os.Parcel;
import android.os.Parcelable;

public class Product implements Parcelable {
	
	public static final String DAY_TOUR = "DayTour";
	public static final String FREE_EASY = "FreeEasy";
	public static final String PACKAGE = "Package";
	
	private int productId;
	private String productName;
	private String productType;
	private String productCountry;
	private String productCity;
	private BigDecimal productLowestPrice;
	private float productRating;
	private String productDefaultImageUrl;
	
	public Product() { }
	
	public Product(int productId, String productName, String productType,
			BigDecimal productLowestPrice, float productRating,
			String productDefaultImageUrl) {
		this.productId = productId;
		this.productName = productName;
		this.productType = productType;
		this.productLowestPrice = productLowestPrice;
		this.productRating = productRating;
		this.productDefaultImageUrl = productDefaultImageUrl;
	}

	public int getProductId() {
		return productId;
	}
	public void setProductId(int productId) {
		this.productId = productId;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getProductType() {
		return productType;
	}
	public void setProductType(String productType) {
		this.productType = productType;
	}
	
	public String getFormattedProductType() {
		String formattedString = "";
		if (productType.equals(DAY_TOUR)) {
			formattedString = "Day Tour";
		}
		else if (productType.equals(FREE_EASY)) {
			formattedString = "Free & Easy";
		}
		else {
			formattedString = "Group Tour";
		}
		
		return formattedString;
	}
	
	public String getProductCountry() {
		return productCountry;
	}
	public void setProductCountry(String productCountry) {
		this.productCountry = productCountry;
	}
	public String getProductCity() {
		return productCity;
	}
	public void setProductCity(String productCity) {
		this.productCity = productCity;
	}
	public BigDecimal getProductLowestPrice() {
		return productLowestPrice;
	}
	public void setProductLowestPrice(BigDecimal productLowestPrice) {
		this.productLowestPrice = productLowestPrice;
	}
	public float getProductRating() {
		return productRating;
	}
	public void setProductRating(float productRating) {
		this.productRating = productRating;
	}
	public String getProductDefaultImageUrl() {
		return productDefaultImageUrl;
	}
	public void setProductDefaultImageUrl(String productDefaultImageUrl) {
		this.productDefaultImageUrl = productDefaultImageUrl;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + productId;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Product other = (Product) obj;
		if (productId != other.productId)
			return false;
		return true;
	}
	
	// Android's Parcel required method from here on
	
    private Product(Parcel in) {
        productId = in.readInt();
        productName = in.readString();
        productType = in.readString();
        productCountry = in.readString();
        productCity = in.readString();
        productLowestPrice = new BigDecimal(in.readString());
        productRating = in.readFloat();
        productDefaultImageUrl = in.readString();
    }
    
	public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel out, int flags) {
        out.writeInt(productId);
        out.writeString(productName);
        out.writeString(productType);
        out.writeString(productCountry);
        out.writeString(productCity);
        if (productLowestPrice != null) {
        	out.writeString(productLowestPrice.toPlainString());
        }
        else {
        	out.writeString("0");
        }
        out.writeFloat(productRating);
        out.writeString(productDefaultImageUrl);
    }

    public static final Parcelable.Creator<Product> CREATOR = new Parcelable.Creator<Product>() {
        public Product createFromParcel(Parcel in) {
            return new Product(in);
        }

        public Product[] newArray(int size) {
            return new Product[size];
        }
    };

}
