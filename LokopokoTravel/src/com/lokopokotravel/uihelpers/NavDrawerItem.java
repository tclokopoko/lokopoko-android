package com.lokopokotravel.uihelpers;

public class NavDrawerItem {
	private String title;
    private int icon;
    private boolean hasChildren;
    private boolean isChildren;
    private String count = "0";
    private boolean isCounterVisible = false;
    
    public NavDrawerItem(String title, int icon, boolean hasChildren, boolean isChildren){
        this.title = title;
        this.icon = icon;
        this.hasChildren = hasChildren;
        this.isChildren = isChildren;
    }
     
    public NavDrawerItem(String title, int icon, boolean hasChildren, boolean isChildren, boolean isCounterVisible, String count){
        this.title = title;
        this.icon = icon;
        this.hasChildren = hasChildren;
        this.isChildren = isChildren;
        this.isCounterVisible = isCounterVisible;
        this.count = count;
    }
     
    public String getTitle(){
        return this.title;
    }
     
    public int getIcon(){
        return this.icon;
    }
     
    public boolean getHasChildren() {
		return hasChildren;
	}
    
    public boolean getIsChildren() {
    	return isChildren;
    }

	public String getCount(){
        return this.count;
    }
     
    public boolean getCounterVisibility(){
        return this.isCounterVisible;
    }
     
    public void setTitle(String title){
        this.title = title;
    }
     
    public void setIcon(int icon){
        this.icon = icon;
    }
    
    public void setHasChildren(boolean hasChildren) {
    	this.hasChildren = hasChildren;
    }
     
    public void setIsChildren(boolean isChildren) {
    	this.isChildren = isChildren;
    }
    
    public void setCount(String count){
        this.count = count;
    }
     
    public void setCounterVisibility(boolean isCounterVisible){
        this.isCounterVisible = isCounterVisible;
    }
}
