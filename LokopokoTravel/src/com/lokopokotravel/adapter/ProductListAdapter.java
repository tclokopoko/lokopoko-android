package com.lokopokotravel.adapter;

import java.util.List;

import com.lokopokotravel.R;
import com.lokopokotravel.model.Product;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.LayerDrawable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.TextView.BufferType;
import android.graphics.PorterDuff;

public class ProductListAdapter extends BaseAdapter {
	private List<Product> listProduct;
	private Context context;

	public ProductListAdapter(Context context, List<Product> listProduct) {
		this.listProduct = listProduct;
		this.context = context;
		
		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
		.threadPriority(Thread.NORM_PRIORITY - 2)
		.denyCacheImageMultipleSizesInMemory()
		.diskCacheFileNameGenerator(new Md5FileNameGenerator())
		.diskCacheSize(50 * 1024 * 1024) // 50 Mb
		.tasksProcessingOrder(QueueProcessingType.LIFO)
		.writeDebugLogs() // Remove for release app
		.build();
		
		// Initialize ImageLoader with configuration.
		ImageLoader.getInstance().init(config);
	}
	
	public List<Product> getList() {
		return listProduct;
	}
	
	public void setList(List<Product> listProduct) {
		this.listProduct = listProduct;
	}
	
	@Override
	public int getCount() {
		return listProduct.size();
	}

	@Override
	public Object getItem(int position) {
		return listProduct.get(position);
	}

	@Override
	public long getItemId(int position) {
		return listProduct.indexOf(getItem(position));
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.list_product, null);
        }
		
		ImageView productImgView = (ImageView)convertView.findViewById(R.id.productImgView);
        TextView productTxtViewTitle = (TextView)convertView.findViewById(R.id.productTxtViewTitle);
        RatingBar productRatingBar = (RatingBar)convertView.findViewById(R.id.productRatingBar);
        TextView productTxtViewPrice = (TextView)convertView.findViewById(R.id.productTxtViewPrice);
        
        final Product product = listProduct.get(position);
        String strPrice = "$" + product.getProductLowestPrice();
        
        if (product.getProductType().equals("DayTour")) {
        	convertView.setBackgroundColor(context.getResources().getColor(R.color.lokopoko_blue));
        }
        else if (product.getProductType().equals("FreeEasy")) {
        	convertView.setBackgroundColor(context.getResources().getColor(R.color.lokopoko_pink));
        }
        else {
        	convertView.setBackgroundColor(context.getResources().getColor(R.color.lokopoko_green));
        }
        
        productImgView.setBackgroundColor(context.getResources().getColor(R.color.lokopoko_bg));
        productTxtViewTitle.setText(product.getProductName());
        productRatingBar.setRating(product.getProductRating());
        
        LayerDrawable stars = (LayerDrawable) productRatingBar.getProgressDrawable();
        stars.getDrawable(0).setColorFilter(context.getResources().getColor(R.color.lokopoko_grey), PorterDuff.Mode.SRC_ATOP);
        stars.getDrawable(1).setColorFilter(context.getResources().getColor(R.color.lokopoko_grey), 
        		PorterDuff.Mode.SRC_ATOP);
        stars.getDrawable(2).setColorFilter(context.getResources().getColor(R.color.lokopoko_light_text_color), 
        		PorterDuff.Mode.SRC_ATOP);
        
		SpannableStringBuilder spanSin = new SpannableStringBuilder();
		SpannableString itemasin = new SpannableString("Starting from\n");
		itemasin.setSpan(new AbsoluteSizeSpan(11, true), 0, itemasin.length(), 0);
		spanSin.append(itemasin);
		SpannableString itemsin = new SpannableString(strPrice);
		itemsin.setSpan(new StyleSpan(android.graphics.Typeface.BOLD), 0, 
				itemsin.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		itemsin.setSpan(new AbsoluteSizeSpan(18, true), 0, itemsin.length(), 0);
		spanSin.append(itemsin);
        productTxtViewPrice.setText(spanSin,BufferType.SPANNABLE);
        
        ImageLoader imageLoader = ImageLoader.getInstance();
        imageLoader.displayImage(product.getProductDefaultImageUrl(), productImgView);
        
        return convertView;
	}
}
