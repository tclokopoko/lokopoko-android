package com.lokopokotravel.adapter;

import java.util.ArrayList;
import java.util.List;
import java.util.SortedSet;

import com.lokopokotravel.R;
import com.lokopokotravel.fragments.SearchResultsFragment;
import com.lokopokotravel.model.Country;

import android.app.Fragment;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.CheckBox;

public class CountryListAdapter extends BaseAdapter {

	private Context context;
	private SearchResultsFragment fragment;
	private List<Country> countryList;

	public CountryListAdapter(Context context, int textViewResourceId,
			List<Country> countryList, SearchResultsFragment fragment) {
		this.context = context;
		this.fragment = fragment;
		this.countryList = countryList;
	}

	private class ViewHolder {
		CheckBox name;
	}

	@Override
	public int getCount() {
		return countryList.size();
	}

	@Override
	public Object getItem(int position) {
		return countryList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return countryList.indexOf(getItem(position));
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		ViewHolder holder = null;
		if (convertView == null) {
			LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = vi.inflate(R.layout.checkbox_selector, null);

			holder = new ViewHolder();
			holder.name = (CheckBox) convertView.findViewById(R.id.ctryChkBox);
			convertView.setTag(holder);
			holder.name.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					CheckBox cb = (CheckBox) v;
					Country country = (Country) cb.getTag();
					country.setSelected(cb.isChecked());
					fragment.refreshCityListView();
				}
			});
		} 
		else {
			holder = (ViewHolder) convertView.getTag();
		}

		Country country = countryList.get(position);
		holder.name.setText(country.getName());
		holder.name.setChecked(country.isSelected());
		holder.name.setTag(country);

		return convertView;
	}
}