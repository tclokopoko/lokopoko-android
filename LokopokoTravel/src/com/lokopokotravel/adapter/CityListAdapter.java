package com.lokopokotravel.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;

import com.lokopokotravel.R;
import com.lokopokotravel.model.City;
import com.lokopokotravel.model.Product;

public class CityListAdapter extends BaseAdapter {

	private Context context;
	private List<City> cityList;

	public CityListAdapter(Context context, int textViewResourceId, List<City> cityList) {
		this.context = context;
		this.cityList = cityList;
	}

	private class ViewHolder {
		CheckBox name;
	}
	
	public List<City> getList() {
		return cityList;
	}
	
	public void setList(List<City> cityList) {
		this.cityList = cityList;
	}

	@Override
	public int getCount() {
		return cityList.size();
	}

	@Override
	public Object getItem(int position) {
		return cityList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return cityList.indexOf(getItem(position));
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		ViewHolder holder = null;
		if (convertView == null) {
			LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = vi.inflate(R.layout.checkbox_selector, null);

			holder = new ViewHolder();
			holder.name = (CheckBox) convertView.findViewById(R.id.ctryChkBox);
			convertView.setTag(holder);
			holder.name.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					CheckBox cb = (CheckBox) v;
					City city = (City) cb.getTag();
					city.setSelected(cb.isChecked());
				}
			});
		} 
		else {
			holder = (ViewHolder) convertView.getTag();
		}

		City city = cityList.get(position);
		holder.name.setText(city.getName());
		holder.name.setChecked(city.isSelected());
		holder.name.setTag(city);

		return convertView;
	}
}