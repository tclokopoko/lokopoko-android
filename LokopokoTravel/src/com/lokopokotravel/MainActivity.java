package com.lokopokotravel;

import java.util.ArrayList;

import com.lokopokotravel.adapter.NavDrawerListAdapter;
import com.lokopokotravel.fragments.SearchFragment;
import com.lokopokotravel.uihelpers.NavDrawerItem;

import android.app.Activity;

import android.app.ActionBar;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentManager.BackStackEntry;
import android.app.FragmentManager.OnBackStackChangedListener;
import android.app.FragmentTransaction;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

public class MainActivity extends Activity implements OnBackStackChangedListener {
	private DrawerLayout mDrawerLayout;
	private ListView mDrawerList;
	private ActionBarDrawerToggle mDrawerToggle;
	private NavDrawerListAdapter adapter;
	
	private CharSequence mDrawerTitle;
	private CharSequence mTitle;
	private String[] navMenuTitles;
	private TypedArray navMenuIcons;
	private ArrayList<NavDrawerItem> navDrawerItems;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		mTitle = mDrawerTitle = getTitle();

		initActionBar();
		
		navMenuTitles = getResources().getStringArray(R.array.nav_drawer_items);
		navMenuIcons = getResources().obtainTypedArray(R.array.nav_drawer_icons);
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerList = (ListView) findViewById(R.id.list_slidermenu);
		navDrawerItems = new ArrayList<NavDrawerItem>();

		navDrawerItems.add(new NavDrawerItem("Home", navMenuIcons.getResourceId(0, -1), false, false));
		navDrawerItems.add(new NavDrawerItem("Asia", navMenuIcons.getResourceId(0, -1), true, false));
		navDrawerItems.add(new NavDrawerItem("Singapore", navMenuIcons.getResourceId(0, -1), false, true));
		navDrawerItems.add(new NavDrawerItem("Malaysia", navMenuIcons.getResourceId(0, -1), false, true)); 
		navDrawerItems.add(new NavDrawerItem("Contact Us", navMenuIcons.getResourceId(0, -1), false, false)); // TODO: change menu icons index
		navMenuIcons.recycle();

		adapter = new NavDrawerListAdapter(getApplicationContext(), navDrawerItems);
		mDrawerList.setAdapter(adapter);

		mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, 
				R.drawable.ic_drawer, R.string.app_name, R.string.app_name) {
			
			@Override
			public boolean onOptionsItemSelected(MenuItem item) {
				if (item != null && item.getItemId() == android.R.id.home) {
	                if (mDrawerLayout.isDrawerOpen(Gravity.RIGHT)) {
	                    mDrawerLayout.closeDrawer(Gravity.RIGHT);
	                } else {
	                    mDrawerLayout.openDrawer(Gravity.RIGHT);
	                }
	            }
	            return false;
			}
			
			public void onDrawerClosed(View view) {
				invalidateOptionsMenu();
			}

			public void onDrawerOpened(View drawerView) {
				invalidateOptionsMenu();
			}
		};
		
		mDrawerLayout.setDrawerListener(mDrawerToggle);

		if (savedInstanceState == null) {
			displayView(0);
		}
		mDrawerList.setOnItemClickListener(new SlideMenuClickListener());
	}
	
	private void initActionBar() {
		ActionBar mActionBar = getActionBar();
		mActionBar.setDisplayShowHomeEnabled(false);
		mActionBar.setDisplayShowTitleEnabled(false);
		
		LayoutInflater mInflater = LayoutInflater.from(this);

		View mCustomView = mInflater.inflate(R.layout.custom_actionbar, null);
		TextView mTitleTextView = (TextView) mCustomView.findViewById(R.id.title_text);
		mTitleTextView.setText(mTitle);

		ImageButton imageButton = (ImageButton) mCustomView.findViewById(R.id.imageButton);
		imageButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				if (mDrawerLayout.isDrawerOpen(Gravity.RIGHT)) {
					mDrawerLayout.closeDrawer(Gravity.RIGHT);
				}
				else {
					mDrawerLayout.openDrawer(Gravity.RIGHT);
				}
			}
		});
		
		ImageButton imageBackButton = (ImageButton) mCustomView.findViewById(R.id.imageBackButton);
		imageBackButton.setVisibility(View.GONE);
		imageBackButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				FragmentManager fragManager = getFragmentManager();
				int lastFragmentIndex =  fragManager.getBackStackEntryCount() - 1;
				
				if (lastFragmentIndex > 0) {
					BackStackEntry backEntry = getFragmentManager().getBackStackEntryAt(lastFragmentIndex);
				    String str = backEntry.getName();
					fragManager.popBackStack(str, FragmentManager.POP_BACK_STACK_INCLUSIVE);
				}
			}
		});

		mActionBar.setCustomView(mCustomView);
		mActionBar.setDisplayShowCustomEnabled(true);
		getFragmentManager().addOnBackStackChangedListener(this);
	
	}

//	@Override
//	public boolean onCreateOptionsMenu(Menu menu) {
////		return false will hide all the menu items as we are building custom actionBar for the drawer
////		getMenuInflater().inflate(R.menu.main, menu);
////		return true;
//		return false;
//	}
//
//	@Override
//	public boolean onPrepareOptionsMenu(Menu menu) {
//		boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
//		menu.findItem(R.id.action_settings).setVisible(!drawerOpen);
//		return super.onPrepareOptionsMenu(menu);
//	}

	@Override
	public void setTitle(CharSequence title) {
		mTitle = title;
		getActionBar().setTitle(mTitle);
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		mDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		mDrawerToggle.onConfigurationChanged(newConfig);
	}

	private class SlideMenuClickListener implements ListView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			displayView(position);
		}
	}
	
	private void displayView(int position) {
        Fragment fragment = null;
        switch (position) {
        case 0:
            fragment = new SearchFragment();
            break;
        case 1:
            //fragment = new DayTourFragment();
            break;
        default:
        	//fragment = new DayTourFragment();
            break;
        }
        
        if (fragment != null) {
            FragmentManager fragmentManager = getFragmentManager();
            fragmentManager.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE); // clear all fragments
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.fragment_container, fragment, fragment.getClass().getSimpleName());
            fragmentTransaction.addToBackStack(fragment.getClass().getSimpleName());
            fragmentTransaction.commit();
            
            mDrawerList.setItemChecked(position, true);
            mDrawerList.setSelection(position);
            setTitle("Lokopoko");
            mDrawerLayout.closeDrawer(mDrawerList);
        }
    }

	@Override
	public void onBackStackChanged() {
		FragmentManager fragManager = getFragmentManager();
		int lastFragmentIndex =  fragManager.getBackStackEntryCount() - 1;
		ActionBar mActionBar = getActionBar();
		View customView = mActionBar.getCustomView();
		ImageButton imageBackButton = (ImageButton) customView.findViewById(R.id.imageBackButton);
		
		if (lastFragmentIndex > 0) {
			imageBackButton.setVisibility(View.VISIBLE);
		}
		else {
			imageBackButton.setVisibility(View.GONE);
		}
	}
}
