package com.lokopokotravel.fragments;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import com.lokopokotravel.R;

import android.app.DatePickerDialog;
import android.app.Fragment;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.MultiAutoCompleteTextView;
import android.widget.Toast;

public class SearchFragment extends Fragment implements OnClickListener {
	private View rootView;
	private MultiAutoCompleteTextView searchMultiAutoComplete;
	private EditText searchEdTxtFromDate;
	private DatePickerDialog fromDatePickerDialog;
	private SimpleDateFormat dateFormatter;
	private Button searchBtnDatePicker;
	private Button searchBtn;
	
	private Date departureDate;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.fragment_search, container, false);

		dateFormatter = new SimpleDateFormat("dd-MMM-yyyy");
		
		searchBtnDatePicker = (Button)rootView.findViewById(R.id.searchBtnDatePicker);
		searchBtnDatePicker.setOnClickListener(this);
		searchBtn = (Button)rootView.findViewById(R.id.searchBtn);
		searchBtn.setOnClickListener(this);
		searchEdTxtFromDate = (EditText)rootView.findViewById(R.id.searchEdTxtFromDate);
		searchEdTxtFromDate.setInputType(InputType.TYPE_NULL);
		searchEdTxtFromDate.setTextIsSelectable(false);
		searchEdTxtFromDate.setOnClickListener(this);
		searchEdTxtFromDate.setOnFocusChangeListener(new OnFocusChangeListener() {
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (hasFocus) { onClick(v); }
			}
		});
		
		initDestinationEditText();
		setDateTimeField();
		
		return rootView;
	}
	
	private void initDestinationEditText() {
		//TODO: Get result from web service
		String[] destinations = { "Singapore", "Malaysia", "Thailand", "Indonesia", "Vietnam", "Cambodia", "Laos" };
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, destinations);
		
		searchMultiAutoComplete = (MultiAutoCompleteTextView) rootView.findViewById(R.id.searchMultiAutoComplete);
		searchMultiAutoComplete.setAdapter(adapter);
		searchMultiAutoComplete.setThreshold(2);
		searchMultiAutoComplete.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());	         
	}
		
	private void setDateTimeField() {
		Calendar currentDate = new GregorianCalendar();
		currentDate.setTime(new Date()); // set minimum date to be today
		Calendar maxDate = new GregorianCalendar();
		maxDate.add(Calendar.YEAR, 1); // set maximum date to be 1 year from now

		Calendar newCalendar = Calendar.getInstance();
		fromDatePickerDialog = new DatePickerDialog(getActivity(), new OnDateSetListener() {
					public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
						Calendar newDate = Calendar.getInstance();
						newDate.set(year, monthOfYear, dayOfMonth);
						departureDate = newDate.getTime();
						searchEdTxtFromDate.setText(dateFormatter.format(newDate.getTime()));
					}
				}, 
				newCalendar.get(Calendar.YEAR),
				newCalendar.get(Calendar.MONTH),
				newCalendar.get(Calendar.DAY_OF_MONTH));

		fromDatePickerDialog.getDatePicker().setMinDate(currentDate.getTimeInMillis());
		fromDatePickerDialog.getDatePicker().setMaxDate(maxDate.getTimeInMillis());
		fromDatePickerDialog.getDatePicker().setCalendarViewShown(false);
		fromDatePickerDialog.getDatePicker().setSpinnersShown(true);
	}
	
	private void navigateToSearchResultsFragment() {
		String stringEditText = searchMultiAutoComplete.getText().toString();
		
		if (stringEditText.length() > 0 && departureDate != null) {
			String [] filters = stringEditText.split(",");
			
			SearchResultsFragment resultsFragment = new SearchResultsFragment(filters, departureDate);
			FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
			fragmentTransaction.replace(R.id.fragment_container, resultsFragment, resultsFragment.getClass().getSimpleName());
			fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
			fragmentTransaction.addToBackStack(resultsFragment.getClass().getSimpleName());
			fragmentTransaction.commit();
		}
		else {
			Toast.makeText(getActivity().getApplicationContext(), "Please enter destination & departure date", Toast.LENGTH_SHORT).show();
		}
		
	}
	
	@Override
	public void onClick(View view) {
		if (view == searchEdTxtFromDate) {
			fromDatePickerDialog.show();
		}
		else if (view == searchBtnDatePicker) {
			fromDatePickerDialog.show();
		}
		else if (view == searchBtn) {
			navigateToSearchResultsFragment();
		}
	}
}
