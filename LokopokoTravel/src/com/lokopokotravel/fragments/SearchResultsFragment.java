package com.lokopokotravel.fragments;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.lokopokotravel.ProductDetailsActivity;
import com.lokopokotravel.R;
import com.lokopokotravel.adapter.CityListAdapter;
import com.lokopokotravel.adapter.CountryListAdapter;
import com.lokopokotravel.adapter.ProductListAdapter;
import com.lokopokotravel.comparator.CityComparator;
import com.lokopokotravel.comparator.CountryComparator;
import com.lokopokotravel.comparator.ProductPriceHighToLowComparator;
import com.lokopokotravel.comparator.ProductPriceLowToHighComparator;
import com.lokopokotravel.comparator.ProductRatingsComparator;
import com.lokopokotravel.model.City;
import com.lokopokotravel.model.Country;
import com.lokopokotravel.model.Product;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.TextView.BufferType;

public class SearchResultsFragment extends Fragment implements OnItemClickListener, OnClickListener, android.widget.RadioGroup.OnCheckedChangeListener {
	private View rootView;
	private CheckBox searchResChkBoxDayTour;
	private CheckBox searchResChkBoxFreeEasy;
	private CheckBox searchResChkBoxPackage;
	private RadioGroup radioGrpSort;
	private Button searchResBtnChangeDate;
	private Button searchResBtnFilter;
	private Button searchResBtnSort;
	private Button dialogBtnFilter;
	
	private Dialog filterDialog;
	private Dialog sortDialog;
	private DatePickerDialog fromDatePickerDialog;

	private List<Product> productList;
	private List<Product> filteredList;
	private List<City> cityList;
	private List<Country> countryList;
	private String[] searchFilters;
	private Date departureDate;
	private int defaultSortOption = R.id.radioPopularity;
	
	private ProductListAdapter productAdapter;
	private CityListAdapter cityAdapter;
	private CountryListAdapter countryAdapter;

	public SearchResultsFragment() { }

	public SearchResultsFragment(String[] searchFilters, Date depatureDate) {
		this.searchFilters = searchFilters;
		this.departureDate = depatureDate;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.fragment_searchresults, container, false);

		searchResChkBoxDayTour = (CheckBox) rootView.findViewById(R.id.searchResChkBoxDayTour);
		searchResChkBoxFreeEasy = (CheckBox) rootView.findViewById(R.id.searchResChkBoxFreeEasy);
		searchResChkBoxPackage = (CheckBox) rootView.findViewById(R.id.searchResChkBoxPackage);
		initTourTypeCheckBox(searchResChkBoxDayTour);
		initTourTypeCheckBox(searchResChkBoxFreeEasy);
		initTourTypeCheckBox(searchResChkBoxPackage);

		searchResBtnChangeDate = (Button) rootView.findViewById(R.id.searchResBtnChangeDate);
		searchResBtnChangeDate.setOnClickListener(this);
		setBtnDateText();

		searchResBtnFilter = (Button) rootView.findViewById(R.id.searchResBtnFilter);
		searchResBtnFilter.setOnClickListener(this);
		searchResBtnSort = (Button) rootView.findViewById(R.id.searchResBtnSort);
		searchResBtnSort.setOnClickListener(this);

		populateList();
		setDateTimeField();
		initSortDialog();

		return rootView;
	}

	private void initTourTypeCheckBox(CheckBox checkBox) {
		checkBox.setChecked(true);
		checkBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				filterListByTourType();
			}
		});
	}

	private void setBtnDateText() {
		Calendar date = Calendar.getInstance();
		date.setTime(departureDate);
		SimpleDateFormat dateFormatter = new SimpleDateFormat("dd MMM yyyy");
		String strDateBtnTitle = "Travel Date: " + dateFormatter.format(date.getTime());
		
		SpannableStringBuilder spanSin = new SpannableStringBuilder();
		SpannableString itemasin = new SpannableString("Change Date\n");
		itemasin.setSpan(new AbsoluteSizeSpan(18, true), 0, itemasin.length(), 0);
		spanSin.append(itemasin);

		SpannableString itemsin = new SpannableString(strDateBtnTitle);
		itemsin.setSpan(new AbsoluteSizeSpan(12, true), 0, itemsin.length(), 0);
		spanSin.append(itemsin);
		searchResBtnChangeDate.setText(spanSin,BufferType.SPANNABLE);
	}
	
	private void initSortDialog() {
		sortDialog = new Dialog(getActivity());
		sortDialog.setContentView(R.layout.custom_sort_dialog);
		sortDialog.setTitle("Sort");
		radioGrpSort = (RadioGroup) sortDialog.findViewById(R.id.dialogRadioGrp);
		radioGrpSort.setOnCheckedChangeListener(this);
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		Product product = filteredList.get(position);
		Intent productDetailsIntent = new Intent(getView().getContext(), ProductDetailsActivity.class);
		productDetailsIntent.putExtra("product", product);
		productDetailsIntent.putExtra("departureDate", departureDate.getTime());
		getActivity().startActivityForResult(productDetailsIntent, 0);
	}

	public void populateList() {

		// TODO: Make web service calls here

		productList = new ArrayList<Product>();

		Product product = new Product();
		product.setProductId(1);
		product.setProductName("Malaysia Durian Trip");
		product.setProductDefaultImageUrl("https://flocations-static.s3.amazonaws.com/images/lokopoko_travel.png");
		product.setProductLowestPrice(new BigDecimal("29.90"));
		product.setProductType("DayTour");
		product.setProductRating(2.0f);
		product.setProductCity("Johor");
		product.setProductCountry("Malaysia");
		productList.add(product);

		Product product1 = new Product();
		product1.setProductId(2);
		product1.setProductName("Singapore Durian Trip");
		product1.setProductDefaultImageUrl("https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xfa1/t31.0-1/c59.703.589.589/s160x160/285989_243290015704710_5824449_o.jpg");
		product1.setProductLowestPrice(new BigDecimal("19.90"));
		product1.setProductType("FreeEasy");
		product1.setProductRating(3.3f);
		product1.setProductCity("Singapore");
		product1.setProductCountry("Singapore");
		productList.add(product1);

		Product product2 = new Product();
		product2.setProductId(3);
		product2.setProductName("Legoland Malaysia");
		product2.setProductDefaultImageUrl("https://flocations-static.s3.amazonaws.com/images/lokopoko_travel.png");
		product2.setProductLowestPrice(new BigDecimal("27.90"));
		product2.setProductType("Package");
		product2.setProductRating(5.0f);
		product2.setProductCity("Kuala Lumpur");
		product2.setProductCountry("Malaysia");
		productList.add(product2);
		
		filteredList = new ArrayList<Product>(productList);

		productAdapter = new ProductListAdapter(getActivity().getApplicationContext(), filteredList);
		ListView listView = (ListView) rootView.findViewById(R.id.searchResults_list);
		listView.setAdapter(productAdapter);
		listView.setOnItemClickListener(this);
		// prgDialog.dismiss();
		
		initFilterDialog();
	}

	private void filterListByTourType() { // i.e Day Tour, Free & Easy, Tour Package
		ArrayList<Product> temp = new ArrayList<Product>();
		filteredList.clear();
		
		for (int i = 0; i < productList.size(); i++) {
			Product product = productList.get(i);
			if (searchResChkBoxDayTour.isChecked()
					&& product.getProductType().equals(Product.DAY_TOUR)) {
				temp.add(product);
			}
			if (searchResChkBoxFreeEasy.isChecked()
					&& product.getProductType().equals(Product.FREE_EASY)) {
				temp.add(product);
			}
			if (searchResChkBoxPackage.isChecked()
					&& product.getProductType().equals(Product.PACKAGE)) {
				temp.add(product);
			}
		}
		
		for (City city : cityList) {
			for (Product product : temp) {
				if ((city.isSelected() && product.getProductCity().equals(city.getName()))) {
					filteredList.add(product);
				}
			}
		}

		//productAdapter.notifyDataSetChanged();
		onCheckedChanged(radioGrpSort, defaultSortOption);
	}

	@Override
	public void onClick(View view) {		
		if (view == searchResBtnChangeDate) {
			fromDatePickerDialog.show();
		} 
		else if (view == searchResBtnFilter) {
			filterDialog.show();
		} 
		else if (view == searchResBtnSort) {
			sortDialog.show();
		}
		else if (view == dialogBtnFilter) {
			filterListByTourType();
			filterDialog.hide();
		}
	}
	
	public void refreshCityListView() {
		Set<City> uniqueCityList = new HashSet<City>();

		for (Country country : countryList) {
			for (City city : cityList) {
				if (!country.isSelected() && city.getCountry().equals(country.getName())) {
					city.setSelected(false);
				}
				if (country.isSelected() && city.getCountry().equals(country.getName())) {
					uniqueCityList.add(city);
				}
			}
		}

		ArrayList<City> filteredCityList = new ArrayList<City>(uniqueCityList);
		cityAdapter.getList().clear();
		cityAdapter.getList().addAll(filteredCityList);
		cityAdapter.notifyDataSetChanged();
	}
	
	private void initFilterDialog() {
		filterDialog = new Dialog(getActivity());
		filterDialog.setContentView(R.layout.custom_filter_dialog);
		filterDialog.setTitle("Filters");

		Set<Country> uniqueCountryList = new HashSet<Country>();
		Set<City> uniqueCityList = new HashSet<City>();
		
		for (int i = 0; i < filteredList.size(); i++) {
			Product product = filteredList.get(i);
			Country country = new Country(product.getProductCountry(), product.getProductCountry(), true);
			City city = new City(product.getProductCity(), product.getProductCountry(), true);
			uniqueCountryList.add(country);
			uniqueCityList.add(city);
		}
		
		countryList = new ArrayList<Country>(uniqueCountryList);
		cityList = new ArrayList<City>(uniqueCityList);
		Collections.sort(countryList, new CountryComparator());
		Collections.sort(cityList, new CityComparator());
		
		countryAdapter = new CountryListAdapter(getActivity().getApplicationContext(),
				R.layout.checkbox_selector, countryList, this);
		cityAdapter = new CityListAdapter(getActivity().getApplicationContext(),
				R.layout.checkbox_selector, new ArrayList(cityList));
		
		ListView ctryListView = (ListView) filterDialog.findViewById(R.id.ctryListView);
		ListView cityListView = (ListView) filterDialog.findViewById(R.id.cityListView);
		
		ctryListView.setAdapter(countryAdapter);
		cityListView.setAdapter(cityAdapter);
		
		dialogBtnFilter = (Button)filterDialog.findViewById(R.id.dialogBtnFilter);
		dialogBtnFilter.setOnClickListener(this);
	}

	private void setDateTimeField() {
		Calendar calendar = new GregorianCalendar();
		calendar.setTime(new Date()); // set minimum date to be today
		Calendar maxDate = new GregorianCalendar();
		maxDate.add(Calendar.YEAR, 1); // set maximum date to be 1 year from now

		Calendar newCalendar = Calendar.getInstance();
		fromDatePickerDialog = new DatePickerDialog(getActivity(), new OnDateSetListener() {
					public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
						Calendar newDate = Calendar.getInstance();
						newDate.set(year, monthOfYear, dayOfMonth);
						departureDate = newDate.getTime();
						setBtnDateText();
					}
				}, 
				newCalendar.get(Calendar.YEAR),
				newCalendar.get(Calendar.MONTH),
				newCalendar.get(Calendar.DAY_OF_MONTH));

		fromDatePickerDialog.getDatePicker().setMinDate(calendar.getTimeInMillis());
		fromDatePickerDialog.getDatePicker().setMaxDate(maxDate.getTimeInMillis());
		calendar.setTime(departureDate);
		fromDatePickerDialog.getDatePicker().updateDate(
				calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
				calendar.get(Calendar.DATE));
		fromDatePickerDialog.getDatePicker().setCalendarViewShown(false);
		fromDatePickerDialog.getDatePicker().setSpinnersShown(true);
	}

	@Override
	public void onCheckedChanged(RadioGroup group, int checkedId) {
		if (group == radioGrpSort) {
			switch (checkedId) {
			case R.id.radioPopularity:
				//TODO
				break;
			case R.id.radioAvgRating:
				Collections.sort(filteredList, new ProductRatingsComparator());
				break;
			case R.id.radioNewness:
				//TODO
				break;
			case R.id.radioPriceLow:
				Collections.sort(filteredList, new ProductPriceLowToHighComparator());
				break;
			case R.id.radioPriceHigh:
				Collections.sort(filteredList, new ProductPriceHighToLowComparator());
				break;
			}
			defaultSortOption = checkedId;
			productAdapter.notifyDataSetChanged();
			sortDialog.hide();
		}
	}
}
