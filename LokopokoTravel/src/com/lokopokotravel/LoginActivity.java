package com.lokopokotravel;

import com.lokopokotravel.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


public class LoginActivity extends Activity {

	private EditText login_TxtUsername;
	private EditText login_TxtPassword;
	private Button login_BtnLogin;
	private Button login_BtnForgetPw;
	private Button login_BtnRegister;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        
        setTitle("Login to Lokopoko");
        login_TxtUsername = (EditText)findViewById(R.id.login_TxtUsername);
        login_TxtPassword = (EditText)findViewById(R.id.login_TxtPassword);
        login_BtnLogin = (Button)findViewById(R.id.login_BtnLogin);
        login_BtnForgetPw = (Button)findViewById(R.id.login_BtnForgetPw);
        login_BtnRegister = (Button)findViewById(R.id.login_BtnRegister);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.login, menu);
        return true;
    }
    
    /*
     * Button Click Events
     * -----------------------------------------------
    */
    
    public void login(View view) {
    	Intent mainIntent = new Intent(getApplicationContext(), MainActivity.class);
		mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		this.startActivity(mainIntent);
		finish();
    }
    
    public void forgetPassword(View view) {
    	
    }
    
    public void register(View view) {
    	
    }
}
