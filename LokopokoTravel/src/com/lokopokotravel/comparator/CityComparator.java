package com.lokopokotravel.comparator;

import java.util.Comparator;

import com.lokopokotravel.model.City;

public class CityComparator implements Comparator<City> {
	
	@Override
	public int compare(City c1, City c2) {
		return c1.getName().compareTo(c2.getName());
	}
}