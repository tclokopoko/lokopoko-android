package com.lokopokotravel.comparator;

import java.util.Comparator;

import com.lokopokotravel.model.Product;

public class ProductRatingsComparator implements Comparator<Product> {

	@Override
	public int compare(Product p1, Product p2) {
		return Float.compare(p1.getProductRating(), p2.getProductRating());
	}
}
