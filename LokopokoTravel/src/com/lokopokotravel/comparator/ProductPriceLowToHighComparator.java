package com.lokopokotravel.comparator;

import java.util.Comparator;

import com.lokopokotravel.model.Product;

public class ProductPriceLowToHighComparator implements Comparator<Product> {

	@Override
	public int compare(Product p1, Product p2) {
		return p1.getProductLowestPrice().compareTo(p2.getProductLowestPrice());
	}
}
