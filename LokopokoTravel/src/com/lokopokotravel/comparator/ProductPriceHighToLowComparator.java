package com.lokopokotravel.comparator;

import java.util.Comparator;

import com.lokopokotravel.model.Product;

public class ProductPriceHighToLowComparator implements Comparator<Product> {

	@Override
	public int compare(Product p1, Product p2) {
		return p2.getProductLowestPrice().compareTo(p1.getProductLowestPrice());
	}
}