package com.lokopokotravel.comparator;

import java.util.Comparator;

import com.lokopokotravel.model.Country;

public class CountryComparator implements Comparator<Country> {
	
	@Override
	public int compare(Country c1, Country c2) {
		return c1.getName().compareTo(c2.getName());
	}
}
