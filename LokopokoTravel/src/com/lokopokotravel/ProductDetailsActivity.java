package com.lokopokotravel;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.Indicators.PagerIndicator;
import com.daimajia.slider.library.Indicators.PagerIndicator.IndicatorVisibility;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.lokopokotravel.model.Product;
import com.lokopokotravel.model.ProductDetails;
import com.lokopokotravel.uihelpers.NumericInputFilters;
import com.nostra13.universalimageloader.core.ImageLoader;

public class ProductDetailsActivity extends Activity implements OnClickListener, BaseSliderView.OnSliderClickListener {

	private static final int minPax = 0;
	private static final int maxPax = 10;
	
	private Date departureDate = new Date();
	
	private View prdDetailsViewPrdType;
	private TextView prdDetailsTxtPrdType;
	private TextView prdDetailsTxtTravelDate;
	private TextView prdDetailsTxtPrdName;
	private TextView prdDetailsTxtPrice;
	private TextView prdDetailsTxtAdultAgeRange;
	private TextView prdDetailsTxtChildAgeRange;
	private TextView prdDetailsTxtInfantAgeRange;
	private TextView prdDetailsTxtDeposit;
	private TextView prdDetailsTxtTotalPrice;
	private RatingBar prdDetailsRatings;
	private EditText prdDetailsTxtAdultPax;
	private EditText prdDetailsTxtChildPax;
	private EditText prdDetailsTxtInfantPax;
	private Button prdDetailsBtnCalculate;
	private Button prdDetailsBtnBookNow;
	
	private SliderLayout prdDetailsImgSlider;
	private ProgressDialog prgDialog;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_product_details);

		Intent intent = getIntent();
		Product product = (Product) intent.getParcelableExtra("product");
		departureDate.setTime(intent.getLongExtra("departureDate", -1));

		setTitle(product.getProductName());
		getActionBar().setDisplayHomeAsUpEnabled(true);

		prdDetailsViewPrdType = (View) findViewById(R.id.prdDetailsViewPrdType);
		prdDetailsTxtPrdType = (TextView) findViewById(R.id.prdDetailsTxtPrdType);
		prdDetailsTxtTravelDate = (TextView) findViewById(R.id.prdDetailsTxtTravelDate);
		prdDetailsTxtPrdName = (TextView) findViewById(R.id.prdDetailsTxtPrdName);
		prdDetailsTxtPrice = (TextView) findViewById(R.id.prdDetailsTxtPrice);
    	prdDetailsTxtAdultAgeRange = (TextView) findViewById(R.id.prdDetailsTxtAdultAgeRange);
    	prdDetailsTxtChildAgeRange = (TextView) findViewById(R.id.prdDetailsTxtChildAgeRange);
    	prdDetailsTxtInfantAgeRange = (TextView) findViewById(R.id.prdDetailsTxtInfantAgeRange);
    	prdDetailsTxtDeposit = (TextView) findViewById(R.id.prdDetailsTxtDeposit);
    	prdDetailsTxtTotalPrice = (TextView) findViewById(R.id.prdDetailsTxtTotalPrice);
    	
    	prdDetailsTxtAdultPax = (EditText) findViewById(R.id.prdDetailsTxtAdultPax);
		prdDetailsTxtChildPax = (EditText) findViewById(R.id.prdDetailsTxtChildPax);
		prdDetailsTxtInfantPax = (EditText) findViewById(R.id.prdDetailsTxtInfantPax);
		prdDetailsTxtAdultPax.setFilters(new InputFilter[]{ new NumericInputFilters(minPax, maxPax)});
		prdDetailsTxtChildPax.setFilters(new InputFilter[]{ new NumericInputFilters(minPax, maxPax)});
		prdDetailsTxtChildPax.setFilters(new InputFilter[]{ new NumericInputFilters(minPax, maxPax)});
		
		prdDetailsRatings = (RatingBar) findViewById(R.id.prdDetailsRatings);
		LayerDrawable stars = (LayerDrawable) prdDetailsRatings.getProgressDrawable();
        stars.getDrawable(0).setColorFilter(this.getApplicationContext().getResources().getColor(R.color.lokopoko_grey), 
        		PorterDuff.Mode.SRC_ATOP);
        stars.getDrawable(1).setColorFilter(this.getApplicationContext().getResources().getColor(R.color.lokopoko_grey), 
        		PorterDuff.Mode.SRC_ATOP);
        stars.getDrawable(2).setColorFilter(this.getApplicationContext().getResources().getColor(R.color.lokopoko_light_text_color), 
        		PorterDuff.Mode.SRC_ATOP);
        
		prdDetailsBtnBookNow = (Button) findViewById(R.id.prdDetailsBtnBookNow);
		prdDetailsBtnCalculate = (Button) findViewById(R.id.prdDetailsBtnCalculate);
		prdDetailsBtnCalculate.setOnClickListener(this);
		prdDetailsBtnBookNow.setOnClickListener(this);

		prgDialog = new ProgressDialog(this);
		prgDialog.setMessage("Retrieving Product Details, please wait...");
		getPrdDetails(product);
	}

	public void getPrdDetails(Product product) {
		// TODO: Make Web Service Calls to retrieve more info
		// prgDialog.setCancelable(false);
		// prgDialog.show();
		
		List<String> images = new ArrayList<String>();
		images.add("https://flocations-static.s3.amazonaws.com/images/lokopoko_travel.png");
		images.add("https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xfa1/t31.0-1/c59.703.589.589/s160x160/285989_243290015704710_5824449_o.jpg");
		
		ProductDetails prdDetails = new ProductDetails(product, 
				images, 
				"(10 and above)", 
				"(2 - 10 years)", 
				"(1 - 23 months)",
				10, 
				new BigDecimal(30));
		
		if (prdDetails.getProductType().equals("DayTour")) {
			prdDetailsViewPrdType.setBackgroundColor(this.getApplicationContext().getResources().getColor(R.color.lokopoko_blue));
        }
        else if (prdDetails.getProductType().equals("FreeEasy")) {
        	prdDetailsViewPrdType.setBackgroundColor(this.getApplicationContext().getResources().getColor(R.color.lokopoko_orange));
        }
        else {
        	prdDetailsViewPrdType.setBackgroundColor(this.getApplicationContext().getResources().getColor(R.color.lokopoko_pink));
        }
		
		prdDetailsTxtPrdType.setText(prdDetails.getFormattedProductType());
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(departureDate);
		prdDetailsTxtTravelDate.setText(new DateFormat().format("dd MMM yyyy", calendar.getTime()));
		prdDetailsTxtPrdName.setText(prdDetails.getProductName());
		prdDetailsTxtPrice.setText("$" + prdDetails.getProductLowestPrice().toString());
		prdDetailsRatings.setRating(prdDetails.getProductRating());
		prdDetailsTxtAdultAgeRange.setText(prdDetails.getAdultRangeText());
		prdDetailsTxtChildAgeRange.setText(prdDetails.getChildRangeText());
		prdDetailsTxtInfantAgeRange.setText(prdDetails.getInfantRangeText());
		
		initImageSlider(prdDetails.getImageUrls());
		// prgDialog.hide();
	}

	public void initImageSlider(List<String> imageUrls) {
		HashMap<String, String> url_maps = new HashMap<String, String>();
		
		for (String url : imageUrls) {
			url_maps.put(url, url);
		}
			
		prdDetailsImgSlider = (SliderLayout) findViewById(R.id.prdDetailsImgSlider);
		for (String name : url_maps.keySet()) {
			DefaultSliderView defaultSliderView = new DefaultSliderView(this);
			defaultSliderView
				.image(url_maps.get(name))
				.setScaleType(BaseSliderView.ScaleType.Fit)
				.setOnSliderClickListener(this);
			defaultSliderView.getBundle().putString("imgUrl", url_maps.get(name));
			prdDetailsImgSlider.addSlider(defaultSliderView);
		}

		prdDetailsImgSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
		prdDetailsImgSlider.setDuration(10000);
	}

	@Override
	public void onClick(View view) {
		if (view == prdDetailsBtnBookNow) {
			// book now button event click
		} 
		else if (view == prdDetailsBtnCalculate) {
			
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem menuItem) {
		switch (menuItem.getItemId()) {
		case android.R.id.home:
			onBackPressed();
			break;
		default:
			break;
		}
		return true;
	}

	@Override
	public void onSliderClick(BaseSliderView slider) {
		String imgUrl = slider.getBundle().getString("imgUrl");
		showImage(imgUrl);
	}

	@Override
	protected void onStop() {
		prdDetailsImgSlider.stopAutoCycle();
		super.onStop();
	}

	public void showImage(String imgUrl) {
		Dialog builder = new Dialog(this);
		builder.requestWindowFeature(Window.FEATURE_NO_TITLE);
		builder.getWindow().setBackgroundDrawable(
				new ColorDrawable(android.graphics.Color.TRANSPARENT));
		builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
			@Override
			public void onDismiss(DialogInterface dialogInterface) {
				// nothing;
			}
		});

		ImageView imageView = new ImageView(this);
		ImageLoader imageLoader = ImageLoader.getInstance();
		imageLoader.displayImage(imgUrl, imageView);
		builder.addContentView(imageView, new RelativeLayout.LayoutParams(
				ViewGroup.LayoutParams.MATCH_PARENT,
				ViewGroup.LayoutParams.MATCH_PARENT));
		builder.show();
	}
}
